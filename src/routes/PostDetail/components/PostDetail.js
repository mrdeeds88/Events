import React, {Component} from 'react'
import {IndexLink, Link} from 'react-router'
import PropTypes from 'prop-types'
import Header from '../../../components/Header'
import Time from 'react-time-format'
import slug from 'slug'
import timeago from 'timeago.js';
import OwlCarousel from 'react-owl-carousel2';

class PostDetail extends Component {
    constructor(props) {
      super(props);
      this.state = {}
    }

    showMap(address) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
        'address': address
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var myOptions = {
            zoom: 18,
            center: results[0].geometry.location,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
          }
          var map = new google.maps.Map(document.getElementById("google-map"), myOptions);

          var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
          });
        }
      });
    }

    updateComponentHeight() {
      this.setState({
        componentHeight: window.innerHeight - 80
      })
    }

    componentDidMount() {
      this.props.fetchPostRequest(this.props.params.id)
      this.props.fetchSubscribersRequest(this.props.params.id)
      this.props.fetchLikesRequest(this.props.params.id)
      this.props.fetchCommentsRequest(this.props.params.id)

      this.updateComponentHeight();
      window.addEventListener("resize", this.updateComponentHeight.bind(this));
    }

    render() {
      const post        = this.props.post
      const subscribers = this.props.subscribers
      const likes       = this.props.likes
      const comments    = this.props.comments

      if(!post.title) {
        return (
          <div>
            <Header
              url = "home"
              icon = 'home'
              />
            <p>Loading...</p>
          </div>
        )
      }

      this.showMap(post.address.street + ' ' + post.address.city + ', ' + post.address.country)

      const owlOptions = {
          items: 1,
          nav: false,
          rewind: true,
          autoplay: false,
          margin: 15,
          stagePadding: 30,
          responsive : {
            480 : {
              stagePadding: 100,
            }
          }
      };
      return(
        <div className="container-fluid post-detail wrap-body">
          <Header
            url = "home"
            icon = 'home'
            />
            <div className="post-item post-item-detail" style={{height: this.state.componentHeight}}>
              <article className="post-header">
                <p><a href="#" className="channel-name">{post.channel.name}</a></p>
                <h3>{post.title}</h3>
                <div className="post-author">
                  <a href="#" className="author">
                    <img className="avatar" src={post.user.avatar} alt=""/>
                  </a>
                  <p>
                    <span className="author-name">{post.user.username}</span>
                    <span className="post-time">Published {timeago().format(post.created)}</span>
                  </p>
                </div>
              </article>

              <div className="tab-panel">
                <ul className="list-inline">
                  <li><a href="#post-content" className="active"><i className="fa fa-info-circle"></i> Detail</a></li>
                  <li><a href="#subscribe-list"><i className="fa fa-group"></i> Participants</a></li>
                  <li><a href="#comment-list"><i className="fa fa-comments-o"></i> Comments</a></li>
                </ul>
              </div>
              {(() => {
                  if(post.images.length) {
                    return(
                      <div className="wrap-slider">
                        <OwlCarousel ref="car" options={owlOptions}>
                          {post.images.map((img, index) =>
                            <div key={index}><img src={img} alt="Event pics"/></div>
                          )}
                        </OwlCarousel>
                      </div>
                    )
                  } else if(post.image) {
                    <div className="wrap-slider">
                      <div><img src="http://media.istockphoto.com/photos/mature-tourist-walking-in-a-city-park-picture-id517458710" alt="The Last of us"/></div>
                    </div>
                  }
              })()}

              <article className="pt0" id="post-content">
                {post.content}
              </article>

              <article className="times-wrap box-entry">
                <h3>When</h3>
                <div className="row">
                  <div className="col-xs-6 text-center start-time">
                    <span><img src="/images/date-from.png" alt=""/></span>
                    <Time value={new Date(post.start)} format="DD/MM/YYYY" /><br />
                    <p><Time value={new Date(post.start)} format="HH:mm" /></p>
                  </div>
                  <div className="col-xs-6 text-center">
                    <span><img src="/images/date-to.png" alt=""/></span>
                    <Time value={new Date(post.end)} format="DD/MM/YYYY" /><br />
                  </div>
                </div>
              </article>

              <article className="map-wrap box-entry">
                <h3>Where</h3>
                <p>
                  <strong>{post.address.name}</strong> <br/>
                  {post.address.street} {post.address.city}, {post.address.zipcode}
                </p>
                <div id="google-map"></div>
              </article>

              <article className="subscribe-list" id="subscribe-list">
                <div>
                  <span><i className="fa fa-check"></i> {subscribers.length} going</span>
                  {subscribers.map((subscriber, index) =>
                    <Link to="#" key={index}><img src={subscriber.user.avatar} alt={subscriber.user.userename} className="avatar"/></Link>
                  )}
                </div>
                <div>
                  <span><i className="fa fa-heart-o"></i> {likes.length} {likes.length > 1 ? 'likes' : 'like'}</span>
                  {likes.map((like, index) =>
                    <Link to="#" key={index}><img src={like.user.avatar} alt={like.user.userename} className="avatar"/></Link>
                  )}
                </div>
              </article>
              <article className="comments" id="comment-list">
                {comments.map((comment, index) =>
                  <div className="comment-item" key={index}>
                    <Link to="#" className="comment-avatar"><img src={comment.user.avatar} alt="" className="avatar"/></Link>
                    <div className="comment-content">
                    <p>
                      <Link to="#"><span><strong>{comment.user.name}</strong> {timeago().format(comment.date)}</span></Link> <br />
                      {comment.content}
                    </p>
                    </div>
                  </div>
                )}
              </article>

            </div>
        </div>
      )
    }
}

PostDetail.contextTypes = {
  store: React.PropTypes.object
}

export default PostDetail
