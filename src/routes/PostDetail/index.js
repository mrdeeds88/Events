import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : '/post/:id/:title',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const PostDetail = require('./containers/PostDetailContainer').default
      const reducer = require('./modules/postDetail').default

      /*  Add the reducer to the store on key 'posts'  */
      injectReducer(store, { key: 'postDetail', reducer })

      /*  Return getComponent   */
      cb(null, PostDetail)

    /* Webpack named bundle   */
  }, 'postDetail')
  }
})
