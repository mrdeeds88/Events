import { connect } from 'react-redux'
import {
  fetchPostRequest,
  fetchPostSuccess,
  fetchPostFailed,
  fetchSubscribersSuccess,
  fetchSubscribersFailed,
  fetchSubscribersRequest,
  fetchLikesSuccess,
  fetchLikesFailed,
  fetchLikesRequest,
  fetchCommentsSuccess,
  fetchCommentsFailed,
  fetchCommentsRequest
} from '../modules/postDetail'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the posts:   */

import PostDetail from '../components/PostDetail'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapDispatchToProps = {
  fetchPostRequest,
  fetchPostSuccess,
  fetchPostFailed,
  fetchSubscribersRequest,
  fetchSubscribersSuccess,
  fetchSubscribersFailed,
  fetchLikesRequest,
  fetchLikesSuccess,
  fetchLikesFailed,
  fetchCommentsRequest,
  fetchCommentsSuccess,
  fetchCommentsFailed
}

const mapStateToProps = (state) => ({
  post: state.postDetail.post,
  subscribers: state.postDetail.subscribers,
  likes: state.postDetail.likes,
  comments: state.postDetail.comments
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const posts = (state) => state.posts
    const tripleCount = createSelector(posts, (count) => count * 3)
    const mapStateToProps = (state) => ({
      posts: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail)
