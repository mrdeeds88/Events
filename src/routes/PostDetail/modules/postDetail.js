import config from '../../../config'
import { browserHistory } from 'react-router'
import {toastr} from 'react-redux-toastr'
import fetch from 'isomorphic-fetch'

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS'
export const FETCH_POST_FAILED  = 'FETCH_POST_FAILED'

export const FETCH_SUBSCRIBERS_SUCCESS = 'FETCH_SUBSCRIBERS_SUCCESS'
export const FETCH_SUBSCRIBERS_FAILED  = 'FETCH_SUBSCRIBERS_FAILED'

export const FETCH_LIKES_SUCCESS = 'FETCH_LIKES_SUCCESS'
export const FETCH_LIKES_FAILED  = 'FETCH_LIKES_FAILED'

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS'
export const FETCH_COMMENTS_FAILED  = 'FETCH_COMMENTS_FAILED'
// ------------------------------------
// Actions
// ------------------------------------

export function fetchPostSuccess (response) {
  return {
    type : FETCH_POST_SUCCESS,
    post : response.res
  }
}
export function fetchPostFailed (response) {
  return {
    type : FETCH_POST_FAILED,
    error: response.error
  }
}
export function fetchSubscribersSuccess (response) {
  return {
    type  : FETCH_SUBSCRIBERS_SUCCESS,
    subscribers : response.res
  }
}
export function fetchSubscribersFailed (response) {
  return {
    type : FETCH_SUBSCRIBERS_FAILED,
    error: response.error
  }
}
export function fetchLikesSuccess (response) {
  return {
    type  : FETCH_LIKES_SUCCESS,
    likes : response.res
  }
}
export function fetchLikesFailed (response) {
  return {
    type : FETCH_LIKES_FAILED,
    error: response.error
  }
}
export function fetchCommentsSuccess (response) {
  return {
    type  : FETCH_COMMENTS_SUCCESS,
    comments : response.res
  }
}
export function fetchCommentsFailed (response) {
  return {
    type : FETCH_COMMENTS_FAILED,
    error: response.error
  }
}

export const fetchPostRequest = (id) => {
  return (dispatch, getState) => {
    var request_url = '/posts/' + id + '/?_expand=user&_expand=channel&_embed=postLikes&_embed=subscribers'
    fetch(config.API_URL + request_url, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${getState().currentUser.hash}`
      }
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        response.json().then(res => {
          const result = {
            res: res
          }
          dispatch(fetchPostSuccess(result))
        })
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchPostFail(response));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export const fetchSubscribersRequest = (id) => {
  return (dispatch, getState) => {
    var request_url = '/subscribers?postId=' + id + '&_expand=user'
    fetch(config.API_URL + request_url, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${getState().currentUser.hash}`
      }
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        response.json().then(res => {
          const result = {
            res: res
          }
          dispatch(fetchSubscribersSuccess(result))
        })
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchSubscribersFailed(response));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export const fetchLikesRequest = (id) => {
  return (dispatch, getState) => {
    var request_url = '/postLikes?postId=' + id + '&_expand=user'
    fetch(config.API_URL + request_url, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${getState().currentUser.hash}`
      }
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        response.json().then(res => {
          const result = {
            res: res
          }
          dispatch(fetchLikesSuccess(result))
        })
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchLikesFailed(response));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export const fetchCommentsRequest = (id) => {
  return (dispatch, getState) => {
    var request_url = '/comments?postId=' + id + '&_expand=user'
    fetch(config.API_URL + request_url, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${getState().currentUser.hash}`
      }
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        response.json().then(res => {
          const result = {
            res: res
          }
          dispatch(fetchCommentsSuccess(result))
        })
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchCommentsFailed(response));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export const actions = {
  fetchPostSuccess,
  fetchPostFailed,
  fetchPostRequest,
  fetchSubscribersSuccess,
  fetchSubscribersFailed,
  fetchSubscribersRequest,
  fetchLikesSuccess,
  fetchLikesFailed,
  fetchLikesRequest,
  fetchCommentsSuccess,
  fetchCommentsFailed,
  fetchCommentsRequest,
}

// ------------------------------------
// Action Handler
// ------------------------------------
const ACTION_HANDLERS = {
  [FETCH_POST_SUCCESS] : (state, action) => {
    return {
      ...state,
      post: action.post
    }
  },
  [FETCH_POST_FAILED] : (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [FETCH_SUBSCRIBERS_SUCCESS] : (state, action) => {
    return {
      ...state,
      subscribers: action.subscribers
    }
  },
  [FETCH_SUBSCRIBERS_FAILED] : (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [FETCH_LIKES_SUCCESS] : (state, action) => {
    return {
      ...state,
      likes: action.likes
    }
  },
  [FETCH_LIKES_FAILED] : (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [FETCH_COMMENTS_SUCCESS] : (state, action) => {
    return {
      ...state,
      comments: action.comments
    }
  },
  [FETCH_COMMENTS_FAILED] : (state, action) => {
    return {
      ...state,
      error: action.error
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  post: {},
  subscribers: [],
  likes: [],
  comments: []
}
export default function postsReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
