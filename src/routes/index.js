// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout'
import FullLayout from '../layouts/FullLayout'
import Home from './Home'
import LoginRoute from './Login'
import PostsRoute from './Posts'
import PostDetailRoute from './PostDetail'

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

function requireAuth (store, replace) {
  const currentUser = JSON.parse(localStorage.getItem('@USER'));
  if (!currentUser) replace('/login')
}

export const createRoutes = (store) => ({
  path: '/',
  childRoutes: [
    // authed route root
    {
      component: FullLayout,
      onEnter: requireAuth,
      indexRoute: PostsRoute(store),
      childRoutes: [
        PostDetailRoute(store)
      ]
    },
    // non-authed routes
    {
      component: FullLayout,
      childRoutes: [
          LoginRoute(store)
      ]
    }
  ]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
