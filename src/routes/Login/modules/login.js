import config from '../../../config'
import { browserHistory } from 'react-router'
import {toastr} from 'react-redux-toastr'

// ------------------------------------
// Constants
// ------------------------------------
export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_ERROR   = 'LOGIN_ERROR'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'

// ------------------------------------
// Actions
// ------------------------------------
export function loginError (error) {
  return {
    error: error,
    type: LOGIN_ERROR
  }
}

export function loginSuccess (response) {
  return (dispatch, getState) => {
    dispatch({
      type    : LOGIN_SUCCESS,
      username: response.username,
      avatar  : response.avatar,
      hash    : response.hash
    });
  };
}

export function loginRequest (username, password) {
  return (dispatch, getState) => {
    fetch(config.API_URL + '/auth', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
    .then(response => response.json())
    .then(data => {
      if (data.authenticated) {
        localStorage.setItem('@USER', JSON.stringify(data.user))
        dispatch(loginSuccess(data.user));
        toastr.success(data.message)

        setTimeout(function(){
          toastr.removeByType('success')
          browserHistory.push('/')
        }, 1500);
      } else {
        toastr.error(data.message)
        setTimeout(function(){
          toastr.removeByType('error')
        }, 3000);
        dispatch(loginError(data.message));
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export const actions = {
  loginError,
  loginRequest,
  loginSuccess
}

// ------------------------------------
// Action Handler
// ------------------------------------
const ACTION_HANDLERS = {
  [LOGIN_SUCCESS]    : (state, action) => {
    return {
      ...state,
      isLoggedIn: true,
      username  : action.username,
      email     : action.email,
      avatar    : action.avatar,
      hash      : action.hash
    }
  },
  [LOGIN_ERROR]    : (state, action) => {
    return {
      ...state,
      isLoggedIn: false,
      error     : action.error
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------

const currentUser = JSON.parse(localStorage.getItem('@USER'));
const initialState = currentUser ? currentUser : {
  username  : '',
  avatar    : '',
  email     : '',
  isLoggedIn: false,
  error     : null,
  hash      : null
}


export default function loginReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
