import React from 'react'
import Posts from '../../Posts/components/Posts'

export const HomeView = () => (
  <div>
    <Posts />
  </div>
)

export default HomeView
