import { connect } from 'react-redux'
import { fetchPostSuccess, fetchPostFailed, fetchPostRequest, toogleFilter, fetchChannelSuccess, fetchChannelFailed, fetchChannelRequest } from '../modules/posts'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the posts:   */

import Posts from '../components/Posts'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapDispatchToProps = {
  fetchPostSuccess,
  fetchPostFailed,
  fetchPostRequest,
  toogleFilter,
  fetchChannelSuccess,
  fetchChannelFailed,
  fetchChannelRequest
}

const mapStateToProps = (state) => ({
  visibilityFilter: state.posts.visibilityFilter,
  posts           : state.posts.posts,
  loadingMore     : state.posts.loadingMore,
  page            : state.posts.page,
  sidebarActive   : state.posts.sidebarActive,
  channels        : state.posts.channels
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const posts = (state) => state.posts
    const tripleCount = createSelector(posts, (count) => count * 3)
    const mapStateToProps = (state) => ({
      posts: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(Posts)
