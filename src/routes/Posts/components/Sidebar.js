import React, {Component} from 'react'

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeChannel: 'All',
      activeChannelName: 'All activities'
    }
    this.props.fetchChannelRequest()
  }

  setFilter(data, event) {
    event.preventDefault()
    const {store} = this.context
    const state   = store.getState()

    this.setState({
      activeChannel: data.channelId,
      activeChannelName: data.channelName
    })

    store.dispatch({
      type: 'SET_FILTER',
      visibilityFilter: {
        ...store.visibilityFilter,
        channel: data.channelId
      }
    })
  }

  searchPosts() {
    const {store} = this.context
    const state   = store.getState()
    this.props.fetchPostRequest()
    store.dispatch({
      type: 'TOGGLE_FILTER'
    })
  }

  render() {
    const {store} = this.context
    const state   = store.getState()
    return (
      <div className="sidebar">
        <div className="sidebar-content">
          <div className="wrap-date-filter">
            <h3 className="title"><span>Date</span></h3>
            <a href="#" className="filter-link">ANYTIME</a>
            <a href="#" className="filter-link">TODAY</a>
            <a href="#" className="filter-link">TOMORROW</a>
            <a href="#" className="filter-link">This week</a>
            <a href="#" className="filter-link">This month</a>
          </div>
          <div className="wrap-channel-filter">
            <h3 className="title"><span>CHANNEL</span></h3>
            <a href="#" onClick={this.setFilter.bind(this, {channelId: 'All', 'channelName': 'All activities'})} className={this.state.activeChannel == 'All' ? "active filter-btn" : "filter-btn"}>All</a>
            {this.props.channels.map(channel=>
              <a href="#"
                key={channel.id}
                onClick={this.setFilter.bind(this, {channelId: channel.id, channelName: channel.name})}
                className={this.state.activeChannel == channel.id ? "active filter-btn" : "filter-btn"}
                >
                {channel.name}
              </a>
            )}
          </div>
        </div>

        <a href="#" onClick={this.searchPosts.bind(this)} className="btn btn-search"><i className="fa fa-search"></i> Search <span>{this.state.activeChannelName}</span></a>
      </div>
    )
  }
}

Sidebar.contextTypes = {
  store: React.PropTypes.object
}

export default Sidebar
