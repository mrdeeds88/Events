import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Header from '../../../components/Header'
import Sidebar from './Sidebar'
import { IndexLink, Link } from 'react-router'
import Time from 'react-time-format'
import slug from 'slug'

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.props.fetchPostRequest()
  }

  updateComponentHeight() {
    this.setState({
      componentHeight: window.innerHeight - 80
    })
  }

  _loadMore(e) {
    e.preventDefault()
    this.props.fetchPostRequest(this.props.page)
  }

  componentDidMount() {
    this.updateComponentHeight();
    window.addEventListener("resize", this.updateComponentHeight.bind(this));
  }

  render() {
    return (
      <div className={this.props.sidebarActive ? 'container-fluid wrap-body sidebar-opened' : 'container-fluid wrap-body'}>
        <Header
          url = "search"
          icon = 'search'
          />
            <div className="post-list" style={{height: this.state.componentHeight}}>
            {this.props.posts.map(post =>
              <article key={post.id} className="post-item" >
                <header>
                  <a href="#" className="author">
                    <img className="avatar" src={post.user.avatar} alt=""/>
                    <span>{post.user.username}</span>
                  </a>
                  <a href="#" className="channel-name">{post.channel != undefined ? post.channel.name : null}</a>
                  <h3><Link to={"/post/" + post.id + '/' + slug(post.title)}>{post.title}</Link></h3>
                  <p className="post-entry">
                    <i className="fa fa-clock-o"></i>
                    <Time value={new Date(post.start)} format="DD/MM/YYYY H:m" /> - <Time value={new Date(post.end)} format="DD/MM/YYYY H:m" />
                  </p>
                </header>
                <div className="content">
                  <p>[No longer than 300 chars] Vivamus sagittis, diam in lobortis, sapien arcu mattis erat, vel aliquet sem urna et risus. Ut feugiat sapien mi potenti...</p>
                </div>
                <footer>
                  <a href="#" className="post-action subscribe"><i className="fa fa-check"></i> {post.subscribers.length} Going!</a>
                  <a href="#" className="post-action like"><i className="fa fa-heart-o"></i> {post.postLikes.length} Likes</a>
                </footer>

              </article>
            )}
            {this.props.loadingMore ? <a href="#" className="loadmore" onClick={this._loadMore.bind(this)}>Load more events</a> : 'No more event :)'}
        </div>
        <Sidebar
          fetchChannelRequest={this.props.fetchChannelRequest}
          channels={this.props.channels}
          fetchPostRequest={this.props.fetchPostRequest}
          />
      </div>
    )
  }
}

Posts.contextTypes = {
  store: React.PropTypes.object
}

export default Posts
