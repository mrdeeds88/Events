import config from '../../../config'
import { browserHistory } from 'react-router'
import {toastr} from 'react-redux-toastr'
import fetch from 'isomorphic-fetch'

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_POSTS_SUCCESS   = 'FETCH_POSTS_SUCCESS'
export const FETCH_POST_FAILED     = 'FETCH_POST_FAILED'

export const TOGGLE_FILTER         = 'TOGGLE_FILTER'
export const SET_FILTER            = 'SET_FILTER'

export const FETCH_CHANNEL_SUCCESS = 'FETCH_CHANNEL_SUCCESS'
export const FETCH_CHANNEL_FAILED  = 'FETCH_CHANNEL_FAILED'

// ------------------------------------
// Actions
// ------------------------------------
export function toogleFilter(){
  return {
    type: TOGGLE_FILTER
  }
}
export function setFilter(){
  return {
    type: SET_FILTER,
    visibilityFilter: {
      channel: '',
      date: ''
    }
  }
}
export function fetchPostSuccess (response) {
  return {
    type       : FETCH_POSTS_SUCCESS,
    posts      : response.res,
    loadingMore: response.loadingMore,
    page       : response.nextPage,
    postType   : response.postType
  }
}
export function fetchPostFailed (response) {
  return {
    type : FETCH_POST_FAILED,
    error: response.error
  }
}
export function fetchChannelSuccess (response) {
  return {
    type     : FETCH_CHANNEL_SUCCESS,
    channels : response.res
  }
}
export function fetchChannelFailed (response) {
  return {
    type : FETCH_CHANNEL_FAILED,
    error: response.error
  }
}


export function fetchPostRequest (page) {
  return (dispatch, getState) => {
    page = page ? page : 1;
    var params = {};
    var postType = 'all';

    if(getState().posts.visibilityFilter.channel != 'All' && getState().posts.visibilityFilter.channel !== '') {
      postType = 'filter';
      params = {
        ...params,
        channelId: getState().posts.visibilityFilter.channel
      }
    }

    var esc   = encodeURIComponent;
    var query = Object.keys(params)
                .map(k => esc(k) + '=' + esc(params[k]))
                .join('&');

    fetch(config.API_URL + '/posts?_expand=user&_expand=channel&_embed=postLikes&_embed=subscribers&_page=' + page + '&' + query, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${getState().currentUser.hash}`
      }
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        var link = response.headers.get('Link');
        var link_array = link.split(',');
        var loadingMore = false;
        if(link_array.length) {
          for (var i = 0; i < link_array.length; i++) {
            var link_children = link_array[i].split(';')
            if(link_children[1] == ' rel="next"') {
              loadingMore = true
              page++
            }
          }
        }

        response.json().then(res => {
          const result = {
            res: res,
            loadingMore: loadingMore,
            nextPage: page,
            postType: postType
          }
          dispatch(fetchPostSuccess(result));
        })
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchPostFail(response));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export function fetchChannelRequest () {
  return (dispatch, getState) => {
    fetch(config.API_URL + '/channels', {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${getState().currentUser.hash}`
      }
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        response.json().then(res => {
          const result = {
            res: res
          }
          dispatch(fetchChannelSuccess(result));
        })
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchChannelFailed(response));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });
  }
}

export const actions = {
  fetchPostSuccess,
  fetchPostFailed,
  fetchPostRequest,
  toogleFilter,
  fetchChannelSuccess,
  fetchChannelFailed,
  fetchChannelRequest
}

// ------------------------------------
// Action Handler
// ------------------------------------
const ACTION_HANDLERS = {
  [FETCH_POSTS_SUCCESS] : (state, action) => {
    if(action.postType)
    return {
      ...state,
      posts      : action.postType == 'all' ? state.posts.concat(action.posts) : action.posts,
      loadingMore: action.loadingMore,
      page       : action.page
    }
  },
  [FETCH_POST_FAILED] : (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [TOGGLE_FILTER] : (state, action) => {
    return {
      ...state,
      sidebarActive: !state.sidebarActive
    }
  },
  [FETCH_CHANNEL_SUCCESS] : (state, action) => {
    return {
      ...state,
      channels      : action.channels
    }
  },
  [FETCH_CHANNEL_FAILED] : (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [SET_FILTER]: (state, action) => {
    return {
      ...state,
      visibilityFilter: action.visibilityFilter
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  visibilityFilter: {
    date: '',
    channel: ''
  },
  posts: [],
  channels: [],
  loadingMore: false,
  page: 1,
  sidebarActive: false,
  postType: ''
}
export default function postsReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
