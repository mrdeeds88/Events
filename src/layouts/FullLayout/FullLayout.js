import React from 'react'
import PropTypes from 'prop-types'
import './FullLayout.scss'
import '../../styles/core.scss'

export const FullLayout = ({ children }) => (
  <div className='full-wrap'>
    {children}
  </div>
)

FullLayout.propTypes = {
  children : PropTypes.element.isRequired
}

export default FullLayout
