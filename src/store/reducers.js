import { combineReducers } from 'redux'
import locationReducer from './location'
import {reducer as toastrReducer} from 'react-redux-toastr'
import loginReducer from '../routes/Login/modules/login'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
    ...asyncReducers,
    toastr: toastrReducer,
    currentUser: loginReducer
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
